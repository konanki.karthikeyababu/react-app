import { createSlice } from "@reduxjs/toolkit";

const usersSlice = createSlice({
  name: "users",
  initialState: {
    list: [],
  },
  reducers: {
    modifyUsers(state, action) {
      state.list = action.payload;
    },
  },
});

export const { modifyUsers } = usersSlice.actions;
export default usersSlice;