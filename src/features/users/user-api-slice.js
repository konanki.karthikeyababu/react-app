import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const apiSlice = createApi({
  reducerPath: "api",
  baseQuery: fetchBaseQuery({
    baseUrl: "http://localhost:9000/api",
  }),
  endpoints(builder) {
    return {
      fetchUsers: builder.query({
        query: (id = "") => ({
          url: `/users`,
          method: "Get",
          params: { id },
        }),
        providesTags: ["UsersList"],
      }),
      addUser: builder.mutation({
        query: (body) => ({
          url: `/users`,
          method: "POST",
          body,
        }),
        invalidatesTags: ["UsersList"],
      }),
      deleteUser: builder.mutation({
        query: (body) => ({
          url: `/users`,
          method: "DELETE",
          body,
        }),
        invalidatesTags: ["UsersList"],
      }),
    };
  },
});

export const { useFetchUsersQuery, useAddUserMutation, useDeleteUserMutation } =
  apiSlice;
