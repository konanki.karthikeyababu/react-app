import { compose, configureStore } from "@reduxjs/toolkit";
import { apiSlice } from "./features/users/user-api-slice";
import usersSlice from "./features/users/usersSlice";

export default configureStore({
  reducer: {
    [apiSlice.reducerPath]: apiSlice.reducer,
    [usersSlice.name]: usersSlice.reducer,
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware().concat(apiSlice.middleware);
  },
});
