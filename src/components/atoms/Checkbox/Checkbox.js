import React from "react";
import "./styles.css";

function Checkbox({ onChange = () => {}, value = "" }) {
  return <input onChange={onChange} type="checkbox" value={value} />;
}

export default Checkbox;
