import React from "react";
import "./styles.css";

function Input({
  type = "text",
  value = "",
  placeholder = "",
  onChange = () => {},
}) {
  return (
    <input
      type={type}
      value={value}
      placeholder={placeholder}
      onChange={onChange}
    ></input>
  );
}

export default Input;
