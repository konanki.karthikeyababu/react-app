import React from "react";
import "./styles.css";

function Button({
  id,
  className = "btn-default",
  disabled = false,
  onClick = () => {},
  label = "",
}) {
  return (
    <button className={className} disable={disabled} onClick={onClick}>
      {label}
    </button>
  );
}

export default Button;
