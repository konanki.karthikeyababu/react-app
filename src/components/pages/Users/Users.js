import { useEffect, useState } from "react";
import {
  useAddUserMutation,
  useDeleteUserMutation,
  useFetchUsersQuery,
} from "../../../features/users/user-api-slice";
import UsersGrid from "../../molecules/UsersGrid/UsersGrid";
import AddUserForm from "../../molecules/AddUserForm/AddUserForm";
import { useDispatch, useSelector } from "react-redux";
import { modifyUsers } from "../../../features/users/usersSlice";
import Button from "../../atoms/Button/Button";
import "./styles.css";

function Users() {
  const [showForm, setShowForm] = useState(false);
  const [selectedUsers, setSelectedUsers] = useState([]);
  const [showDelete, setShowDelete] = useState(false);

  const usersList = useSelector((state) => state.users.list);

  const dispatch = useDispatch();

  const { data: users = [], isFetching, error } = useFetchUsersQuery();
  const [createUser] = useAddUserMutation();
  const [deleteUser] = useDeleteUserMutation();

  useEffect(() => {
    if (!isFetching && !error) {
      dispatch(modifyUsers(users));
    }
  }, [isFetching, error, users]);

  useEffect(() => {
    console.table(users);
  });

  function addUser() {
    setShowForm(true);
  }

  function handleAddNewUserFormSubmit(newUser) {
    createUser(newUser);
  }

  function handleFormClose() {
    setShowForm(!showForm);
  }

  function handleDeleteUsers() {
    console.table(selectedUsers);
    deleteUser({ userIds: selectedUsers });
    setSelectedUsers([]);
  }

  const showAddBtn =
    isFetching || showDelete ? null : (
      <Button onClick={addUser} label="Add User" />
    );

  const showDeleteBtn =
    usersList.length > 0 && !showDelete ? (
      <Button onClick={() => setShowDelete(true)} label="Delete users" />
    ) : null;

  const deleteCtrlBtns = showDelete ? (
    <>
      <Button
        onClick={() => {
          setShowDelete(false);
          setSelectedUsers([]);
        }}
        label="Cancel"
      />

      <Button
        className="delete-btn"
        disabled={!(selectedUsers.length > 0)}
        onClick={handleDeleteUsers}
        label="Delete"
      />
    </>
  ) : null;

  return (
    <div className="App">
      <header className="App-header">
        <UsersGrid
          isFetching={isFetching}
          selectedUsers={selectedUsers}
          setSelectedUsers={setSelectedUsers}
          showCheckBox={showDelete}
        />
        {showForm ? (
          <AddUserForm
            handleFormClose={handleFormClose}
            handleAddNewUserFormSubmit={handleAddNewUserFormSubmit}
          />
        ) : (
          <div>
            {showAddBtn}
            {showDeleteBtn}
            {showDelete ? deleteCtrlBtns : null}
          </div>
        )}
      </header>
    </div>
  );
}

export default Users;
