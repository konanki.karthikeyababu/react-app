import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import "./styles.css";
import { useFetchUsersQuery } from "../../../features/users/user-api-slice";

function UserDetails() {
  const usersList = useSelector((state) => state.users.list);
  const [user, setUser] = useState({});

  const { userId } = useParams();

  const { data: userDetails, isFetching } = useFetchUsersQuery(userId);

  useEffect(() => {
    // const filteredUser = usersList.filter(({ id }) => id == userId)[0];
    setUser(userDetails);
  }, [isFetching]);

  return (
    <div className="App">
      <header className="App-header">
        <div className="details-container">
          <p className="user-info">
            Name: <span>{user && user.name}</span>
          </p>
          <p className="user-info">
            Email: <span>{user && user.email}</span>
          </p>
          <p className="user-info">
            Address: <span>{user?.address?.street}</span>
          </p>
        </div>
      </header>
    </div>
  );
}

export default UserDetails;
