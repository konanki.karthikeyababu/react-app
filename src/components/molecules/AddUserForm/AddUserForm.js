import { useState } from "react";
import { nanoid } from "@reduxjs/toolkit";
import Button from "../../atoms/Button/Button";
import Input from "../../atoms/Input/Input";
import "./styles.css";

function AddUserForm({ handleFormClose, handleAddNewUserFormSubmit }) {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [street, setStreet] = useState("");

  function handleFormSubmit(e) {
    e.preventDefault();

    const newObj = {
      name,
      email,
      address: {
        street,
      },
    };

    if (name && email) {
      handleAddNewUserFormSubmit(newObj);
      setName("");
      setEmail("");
      setStreet("");
      handleFormClose();
    }
  }

  return (
    <form className="usr-input-form">
      <div className="inputs-container">
        <Input
          value={name}
          placeholder="Name"
          onChange={(e) => setName(e.target.value)}
        />
        <Input
          value={email}
          placeholder="Email"
          onChange={(e) => setEmail(e.target.value)}
        />
        <Input
          value={street}
          placeholder="Street"
          onChange={(e) => setStreet(e.target.value)}
        />
      </div>
      <div className="form-btns-wrapper">
        <Button onClick={handleFormSubmit} label="Submit" />
        <Button onClick={handleFormClose} label=" X " />
      </div>
    </form>
  );
}

export default AddUserForm;
