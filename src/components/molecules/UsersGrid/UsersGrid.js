import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import Checkbox from "../../atoms/Checkbox/Checkbox";
import "./styles.css";

function UsersGrid({
  isFetching,
  selectedUsers,
  setSelectedUsers,
  showCheckBox,
}) {
  const [status, setStatus] = useState("");
  const usersList = useSelector((state) => state.users.list);
  const navigate = useNavigate();

  useEffect(() => {
    if (!isFetching && !usersList.length > 0) {
      setStatus("No Data");
    } else {
      setStatus("Loading...");
    }
  }, [isFetching, usersList.length]);

  function handleCheckboxChange(e) {
    const value = e.target.value;
    if (e.target.checked) {
      setSelectedUsers([...selectedUsers, value]);
    } else {
      setSelectedUsers(selectedUsers.filter((id) => id !== value));
    }
  }

  return usersList && usersList.length > 0 ? (
    <>
      <table>
        <tbody>
          <tr>
            <th>User Name</th>
            <th>Email</th>
            <th>Address</th>
          </tr>
          {usersList.map(({ id, name, email, address: { street } }) => {
            const className = selectedUsers.includes(id.toString())
              ? "ckecked"
              : "";

            return (
              <tr key={id}>
                <td
                  onClick={() => navigate("/users/" + id)}
                  className={className}
                >
                  {name}
                </td>
                <td
                  onClick={() => navigate("/users/" + id)}
                  className={className}
                >
                  {email}
                </td>
                <td
                  onClick={() => navigate("/users/" + id)}
                  className={className}
                >
                  {street}
                </td>
                <td>
                  {showCheckBox ? (
                    <Checkbox
                      onChange={handleCheckboxChange}
                      type="checkbox"
                      value={id}
                    />
                  ) : null}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  ) : (
    <h1>{status}</h1>
  );
}

export default UsersGrid;
