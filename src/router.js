import React from "react";
import { createBrowserRouter } from "react-router-dom";
import NavBar from "./components/organisms/NavBar/NavBar";
import Users from "./components/pages/Users/Users";
import UserDetails from "./components/pages/UserDetails/UserDetails";

const router = createBrowserRouter([
  {
    path: "/",
    element: <NavBar />,
    children: [
      {
        path: "/",
        element: <h1>Home</h1>,
      },
      {
        path: "users",
        element: <Users />,
      },
      {
        path: "users/:userId",
        element: <UserDetails />,
      },
    ],
  },
]);

export default router;
