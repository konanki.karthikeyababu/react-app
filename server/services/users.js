const db = require("./db");

async function getUsers() {
  const rows = await db.query(`SELECT id, name, email, street FROM users`);
  const data = rows ? rows : [];

  return data;
}

async function getUser(id) {
  const rows = await db.query(`SELECT * FROM users WHERE id=${id}`);
  return rows;
}

async function addUser({ name, street, email }) {
  const response = await db.query(
    `INSERT INTO users(name, street, email) VALUES('${name}', '${street}', '${email}')`
  );

  return response;
}

async function deleteUser(userIds) {
  const response = await db.query(
    `DELETE FROM users WHERE id IN (${userIds.join(",")})`
  );
  return response;
}

module.exports = {
  getUsers,
  getUser,
  addUser,
  deleteUser,
};
