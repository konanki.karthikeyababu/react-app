const router = require("express")();
const mysql = require("mysql2/promise");
const {
  getUsers,
  getUser,
  addUser,
  deleteUser,
} = require("../../services/users");

router.get("/", async function (req, res, next) {
  function modifyUser(item) {
    item.address = {
      street: item.street,
    };
    delete item["street"];
    return item;
  }
  try {
    if (req?.query?.id) {
      const response = await getUser(req.query.id);
      if (response.length) {
        const [modifiedResponse] = response.map(modifyUser);
        res.send(modifiedResponse);
      } else {
        res.status(404).send({
          message: "Invalied ID",
        });
      }
    } else {
      const response = await getUsers();
      const modifiedResponse = response.map(modifyUser);
      res.json(modifiedResponse);
    }
  } catch (err) {
    console.error(`Error while getting users `, err.message);
    next(err);
  }
});

router.post("/", async function (req, res, next) {
  try {
    const payload = {
      ...req.body,
      ...req.body.address,
    };
    delete payload.address;

    const data = await addUser(payload);
    res.json(data);
  } catch (err) {
    console.error(`Error while posting user`, err.message);
    next(err);
  }
});

router.delete("/", async function (req, res, next) {
  try {
    const userIds = req.body.userIds;
    const response = await deleteUser(userIds);
    res.json(response);
  } catch (err) {
    console.error(`Error while posting user`, err.message);
    next(err);
  }
});

module.exports = router;
