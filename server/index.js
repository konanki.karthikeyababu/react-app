const express = require("express");
const cors = require("cors");
const usersApi = require("./routes/users/index");

const app = express();
const port = 9000;

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use("/api/users", usersApi);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
